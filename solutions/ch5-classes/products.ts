
class Product {
    constructor(
        private _name: string = '',
        private _count: number = 0,
        private _price: number = 0
    ) { }

    get name(): string { return this._name; }
    get itemsInStock(): number { return this._count; }
    get price(): number { return this._price; }

    private static nextId: number = 1;
    static mk(): Product {
        const id: number = Product.nextId++;
        const count: number = Math.round(10 * Math.random());
        const price: number = Math.round(100 + 1000 * Math.random());
        return new Product('prod-' + id, count, price);
    }
}

const N = 1000;
let products: Product[] = [];
for (let k = 1; k <= N; ++k) products.push(Product.mk());
//products.forEach(p => console.log(p));

let averagePrice = products.map(p => p.price).reduce((sum, p) => sum + p) / products.length;
console.log('average price: SEK', averagePrice);

console.log('--- out of stock ---');
let outOfStock = products.filter(p => p.itemsInStock === 0);
outOfStock.forEach(p => console.log(p));
