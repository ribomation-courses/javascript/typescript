export class Queue<T> {
    private q: T[] = [];
    private readonly max: number;
    constructor(capacity: number = 10) { this.max = capacity; }

    size(): number { return this.q.length; }
    empty(): boolean { return this.size() === 0; }
    full(): boolean { return this.size() === this.max; }

    put(x: T): void {
        if (this.full()) throw new Error('queue full');
        this.q.push(x);
    }
    get(): T {
        if (this.empty()) throw new Error('queue empty');
        return this.q.shift() as T;
    }
}

const N = 12;
{    
    const numbers = new Queue<number>(N);
    for (let k = 1; !numbers.full(); ++k) numbers.put(k);
    while (!numbers.empty()) console.log(numbers.get());
}

{    
    const strings = new Queue<string>(N);
    for (let k = 1; !strings.full(); ++k) strings.put(`str-${k}`);
    while (!strings.empty()) console.log(strings.get());
}


