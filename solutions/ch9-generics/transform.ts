
function transform<T>(lst: T[], f: (t: T) => T): T[] {
    for (let k in lst) lst[k] = f(lst[k]);
    return lst;
}

console.log('numbers:', transform<number>([1, 2, 3, 4, 5], n => n * n));
console.log('strings:', transform(['ts', 'is', 'cool'], s => s.toUpperCase()));
