import { Cache } from './cache';

class Numbers {
    private static _fibonacci(n: number): number {
        if (n < 0) throw new Error('negative arg: ' + n);
        if (n === 0) return 0;
        if (n === 1) return 1;
        return Numbers._fibonacci(n - 2) + Numbers._fibonacci(n - 1);
    }

    @Cache()
    static fibonacci(n: number): number {
        return Numbers._fibonacci(n);
    }
}

console.log('fib(%d) = %s', 42, Numbers.fibonacci(42));
console.log('fib(%d) = %s', 42, Numbers.fibonacci(42));

