
export function Cache() {
    interface CacheEntry {
        arg: number; retval: number
    }
    let cache: CacheEntry[] = [];

    return function (obj: Object, name: string, ctx: TypedPropertyDescriptor<any>) {
        const func = ctx.value;

        ctx.value = function (...args: any[]) {
            let arg = args[0] || -1;
            console.log('%s(%d)...', name, arg);

            const entry = cache.filter(e => e.arg === arg);
            let retval;
            if (entry && entry.length > 0) {
                retval = entry[0].retval;
            } else {
                retval = func.apply(this, args);
                cache.push({ arg, retval });
            }

            console.log('%s(%d) -> %d', name, arg, retval);
            return retval;
        };

        return ctx;
    };
}

