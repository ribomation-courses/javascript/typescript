function sum(n: number): number {
    return n * (n + 1) / 2;
}

for (let k = 10; k <= 1000; k *= 10) {
    [1, 2, 5].forEach(n => console.log('%d: %d', k * n, sum(k * n)));
}
