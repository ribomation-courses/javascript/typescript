enum AccountType { CHECK, SAVE, INVEST };

function mkAccount(
    type: AccountType = AccountType.CHECK,
    owner: string = 'no name',
    balance: number = 100,
    rate: number = 1.5,
    overdrawable: boolean = true
) {
    return {type,owner,balance,rate,overdrawable};
}

const accounts = [
    mkAccount(AccountType.INVEST, 'Anna Conda', 10000, 0.25, false),
    mkAccount(AccountType.SAVE, 'Per Silja'),
    mkAccount(),
];

accounts.forEach(acc => console.log(JSON.stringify(acc)));

