type WhatEver = string | number;

function transform(lst: WhatEver[], f: (x: WhatEver) => WhatEver): WhatEver[] {
    for (let k in lst) lst[k] = f(lst[k]);
    return lst;
}

console.log(transform([1, 2, 3, 4, 5], x=> (x as number)*(x as number)));
console.log(transform(['ts', 'is', 'cool'], x => (<string>x).toUpperCase()));

//console.log(transform([true], x => (<string>x).toUpperCase()));


