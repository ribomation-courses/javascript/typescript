
export abstract class Shape {
    abstract area(): number;
    toString() {
        return `${(<any>this).constructor.name}: area=${this.area().toFixed(2)}`;
    }
}

export class Rect extends Shape {
    constructor(private _width: number, private _height: number) { super(); }
    area(): number { return this._width * this._height; }
}

export class Square extends Shape {
    constructor(private _side: number) { super(); }
    area(): number { return this._side * this._side; }
}

export class Circle extends Shape {
    constructor(private _radius: number) { super(); }
    area(): number { return this._radius * this._radius * Math.PI; }
}

export class Triangle extends Shape {
    constructor(private _base: number, private _height: number) { super(); }
    area(): number { return this._base * this._height / 2; }
}

function mkShape(): Shape {
    const type = Math.round(4 * Math.random());
    const a = 10 * Math.random() + 1;
    const b = 10 * Math.random() + 1;
    switch (type % 4) {
        case 0: return new Rect(a, b);
        case 1: return new Square(a);
        case 2: return new Circle(a);
        case 3: return new Triangle(a, b);
    }
    throw new Error('Unexpected type=' + type);
}

function mkShapes(n: number = 10): Shape[] {
    let shapes: Shape[] = [];
    while (--n > 0) shapes.push(mkShape());
    return shapes;
}

mkShapes(25).forEach(s => console.log(s.toString()));



