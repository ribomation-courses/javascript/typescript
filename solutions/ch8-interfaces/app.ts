import * as FS from 'fs';
import * as P from 'process';
import { User } from './user';

const filename = P.argv[2] || './users.json';
const txt = FS.readFileSync(filename, 'utf8').toString();
const users: User[] = JSON.parse(txt);
//console.log('users: ', users);

const bizUsers = users.filter(u => u.email.lastIndexOf('.biz') > -1);
//console.log('BIZ users: ', bizUsers);
bizUsers.forEach(u => console.log('%s: %s (%s)', 
u.name, u.email, u.address.city));
