export class Product {
    constructor(
        private _name: string = '',
        private _count: number = 0,
        private _price: number = 0
    ) { }

    get name(): string { return this._name; }
    get itemsInStock(): number { return this._count; }
    get price(): number { return this._price; }

    private static nextId: number = 1;
    static mk(): Product {
        const id: number = Product.nextId++;
        const count: number = Math.round(10 * Math.random());
        const price: number = Math.round(100 + 1000 * Math.random());
        return new Product('prod-' + id, count, price);
    }
}
