export enum AccountType { CHECK, SAVE, INVEST };

export class Account {
    public static rate: number = 2;
    constructor(
        private _balance: number = 100,
        private _type: AccountType = AccountType.CHECK,
        private _owner: string = 'no name'
    ) { }
    get type(): AccountType { return this._type; }
    get owner(): string { return this._owner; }
    get balance(): number { return this._balance; }
    raise(): number {
        this._balance *= 1 + Account.rate / 100;
        return this._balance;
    }
    toString(): string {
        return `Account{SEK ${this.balance}, type=${AccountType[this.type]}, owner=${this.owner}}`;
    }
}

