import * as FS from 'fs';
import * as P from 'process';

const filename = P.argv[2] || './word-count.ts';
const txt = FS.readFileSync(filename, 'utf8').toString();
const words = txt.split(/[^a-z]+/i);

console.log('Number of words in "%s" are %d', filename, words.length);

