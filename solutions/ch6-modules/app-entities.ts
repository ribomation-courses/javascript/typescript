import { Account, AccountType } from './account';
import { Product } from './product';

function usingAccount() {
    console.log('--- Using Account ---');
    const accounts: Account[] = [
        new Account(500, AccountType.SAVE, 'Per Silja'),
        new Account(1000, AccountType.INVEST),
        new Account(1500),
        new Account()
    ];

    accounts.forEach(acc => console.log(acc.toString()));
    console.log('*** raise()');
    accounts.forEach(acc => acc.raise());
    accounts.forEach(acc => console.log(acc.toString()));
}

function usingProduct(N: number = 10) {
    console.log('--- Using Product ---');
    let products: Product[] = [];
    for (let k = 1; k <= N; ++k) products.push(Product.mk());
    //products.forEach(p => console.log(p));

    let averagePrice = products.map(p => p.price).reduce((sum, p) => sum + p) / products.length;
    console.log('average price: SEK', averagePrice);

    console.log('--- out of stock ---');
    let outOfStock = products.filter(p => p.itemsInStock === 0);
    outOfStock.forEach(p => console.log(p));
}

usingAccount();
usingProduct(50);
