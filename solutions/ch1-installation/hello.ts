import * as P from 'process';
//console.log('args', P.argv);

const firstName: string = P.argv[2] || 'Nisse';
const num: number = (parseInt(P.argv[3], 10) || 45) * 3;

console.log(`Hi ${firstName.toUpperCase()}, magic number is ${num} !!`);
