TypeScript, 2 days
====
Welcome to this course.
The syllabus can be found at
[javascript/typescript](https://www.ribomation.se/courses/javascript/typescript.html)

Here you can find
* Installation instructions
* Solutions to the exercises

Installation Instructions
====
In order to do the programming exercises of the course, 
you need to have the following software installed:

* [Node JS](https://nodejs.org/en/download/)
* [Microsoft Visual Code](https://code.visualstudio.com/) or
    * [JetBrains WebStorm (_30 days trial_)](https://www.jetbrains.com/webstorm/download)
* [Google Chrome](https://www.google.se/chrome/browser/desktop/)
* [GIT Client](https://git-scm.com/downloads)

During the course we will install additional software, such as the TypesScript compiler, using NPM.

Course GIT Repo
====
It's recommended that you keep the git repo and your solutions separated. 
Create a dedicated directory for this course and a sub-directory for 
each chapter. Get the course repo initially by a `git clone` operation

    mkdir -p ~/typescript-course/my-solutions
    cd ~/typescript-course
    git clone https://gitlab.com/ribomation-courses/javascript/typescript.git gitlab

Get any updates by a `git pull` operation

    cd ~/typescript-course/gitlab
    git pull



***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

